| URI                      | Opération | MIME                                                         | Requête             | Réponse                                                   |
| :----------------------- | :-------- | :----------------------------------------------------------- | :------------------ | :-------------------------------------------------------- |
| /pizzas                  | GET       | <-application/json<br><-application/xml                      |                     | liste des pizzas                                          |
| /pizzas/{id}             | GET       | <-application/json<br><-application/xml                      |                     | une pizza ou 404                                          |
| /pizzas/{id}/name        | GET       | <-text/plain                                                 |                     | le nom de la pizza ou 404                                 |
| /pizzas                  | POST      | <-/->application/json<br>->application/x-www-form-urlencoded | Liste d'ingrédients | Nouvelle pizza <br>409 si la pizza existe déjà (même nom) |
| /pizzas/{id}             | DELETE    |                                                              |                     |                                                           |
| /pizzas/{id}/ingredients | GET       | <-application/json<br><-application/xml                      |                     |  Liste d'ingrédients de la pizza ou 404                   |
