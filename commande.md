| URI                      | Opération | MIME                                                         | Requête             | Réponse                                                   |
| :----------------------- | :-------- | :----------------------------------------------------------- | :------------------ | :-------------------------------------------------------- |
| /commandes                  | GET       | <-application/json<br><-application/xml                      |                     | liste des commandes                                          |
| /commandes/{id}             | GET       | <-application/json<br><-application/xml                      |                     | une commande ou 404                                          |
| /commandes/{id}/name        | GET       | <-text/plain                                                 |                     | le nom de la commande ou 404                                 |
| /commandes                  | POST      | <-/->application/json<br>->application/x-www-form-urlencoded | Liste de commande | Nouvelle commande <br>409 si la commande existe déjà (même nom) |
| /commandes/{id}             | DELETE    |                                                              |                     |                                                           |
| /commandes/{id}/pizzas | GET       | <-application/json<br><-application/xml                      |                     |  Liste de pizza de la commande ou 404                   |
