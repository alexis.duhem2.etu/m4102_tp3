package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
	private UUID id;
	private String name;
	private List<Ingredient> ingredient; 
	 
	public PizzaDto() {
	}

	public void setIngredient(List<Ingredient> ingredients) {
		this.ingredient = ingredients;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public List<Ingredient> getIngredient() {
		return ingredient;
	}
	

}
