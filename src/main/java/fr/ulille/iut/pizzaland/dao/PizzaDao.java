package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas ("
			+ "id VARCHAR(128) PRIMARY KEY,"
			+ "name VARCHAR UNIQUE NOT NULL"
			+ ")")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation ("
			+ "pizzaId VARCHAR(128),"
			+ "ingredientID VARCHAR(128),"
			+ "FOREIGN KEY (pizzaId) REFERENCES Pizzas(id),"
			+ "FOREIGN KEY (ingredientId) REFERENCES ingredients(id)"
			+ ")")

	void createAssociationTable();

	@SqlUpdate("DROP TABLE IF EXISTS Pizzas")
	void dropPizzaTable();

	@SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
	void dropPizzaIngredientsAssociationTable();

	@Transaction
	default void createTableAndIngredientAssociation() {
		createAssociationTable();
		createPizzaTable();
	}

	@Transaction
	default void dropTableAndIngredientAssociation() {
		dropPizzaIngredientsAssociationTable();
		dropPizzaTable();
	}

	@Transaction
	default void insert(){

	}
	@Transaction
	default Pizza getTableAndIngredientAssociation(UUID id) {
		Pizza p=findById(id);
		List<UUID> ingredientsID=getAllIngredientsID(id);
		p.setIngredients(getAllIngredients(ingredientsID));
		return p;
	}

	@Transaction
	default List<Ingredient> getAllIngredients(List<UUID> idIngredients){
		IngredientDao ingredients = BDDFactory.buildDao(IngredientDao.class);
		List<Ingredient> ingredient=new ArrayList<>();
		for(UUID id: idIngredients) {
			ingredient.add(ingredients.findById(id));
		}
		return ingredient;
	}



	@SqlQuery("SELECT * FROM Pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();

	@SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(@Bind("id") UUID id);


	@SqlQuery("SELECT * FROM Pizzas WHERE name= :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(@Bind("name") String name);

	@SqlUpdate("INSERT INTO Pizzas (id,name) Values (:id,:name)")
	void insertPizza(@BindBean Pizza p);

	
	@SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
	void remove(@Bind("id") UUID id);
	
	@SqlQuery("Select * from ingredients where id in(SELECT ingredientID FROM PizzaIngredientsAssociation WHERE pizzaID = :id)")
	@RegisterBeanMapper(Ingredient.class)
	List<Ingredient> findIngredientsById(@Bind("id") UUID id);

	default void insertTablePizzaAndIngredientAssociation(Pizza pizza) {
		this.insertPizza(pizza);
		for(Ingredient ingr : pizza.getIngredients()) {
			this.insertPizzaAndIngredientAssociation(pizza,ingr);
		}
	}
	
	@SqlQuery("SELECT ingredientID FROM PizzaIngredientsAssociation where pizzaID=:idPizza")
	@RegisterBeanMapper(Pizza.class)
	List<UUID> getAllIngredientsID(@Bind("idPizza") UUID idPizza);
	
	@SqlUpdate("INSERT INTO PizzaIngredientsAssociation (pizzaID,ingredientID) VALUES( :pizza.id, :ingredient.id)")
    void insertPizzaAndIngredientAssociation(@BindBean("pizza") Pizza pizza ,@BindBean("ingredient") Ingredient ingredient);


}

